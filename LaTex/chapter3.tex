\section{Introduction}
In the previous chapter, we studied the relationships between rhythmic gestures and some speech gestures. We also established that melodic gestures occupy a separate space and can be dealt with on their own. In this chapter, we propose a system to convert melodic gestures into an interface for computational creativity, which composes music according to phrases drawn by hand. This interface may aid ideation in musical phrases and to create novelty from the musical material of a raga. We start with understanding how melodic plotting is understood as a notational concept as opposed to just a representation. We then experiment with melodic envelopes and melodic plotting, and use this data to create a classifier for creating a generative music interface based on a constraint satisfaction model.


\section{Experiment 1: Graphical Notation} 
Meer et al . (2010) elaborate a scheme of notation system based on plotting pitch movements against time to produce analysis outputs titled 'melograph's. Their project emphasizes the need for a new kind of notation system that clearly represent pitch transitions. Despite the difficulties in making melographs from recorded music due to the overlapping of different instruments, lack of clarity and differences in intensity, the melograph system surely contains far more information than plain notation. 

This notation system is one of many modification proposed by modern musicologists to better include ornaments specific to Indian music. Some other examples can be illustrated in Magriel (2008) and Bor, J (2002). Many musicologists also prefer to simply use Western notation where a lot more granularity can be represented than just in Bhatkhande type notation as seen in Jairazbhoy (1995). Meer et al’s melograph system of notation is the one that comes closest to graphical notation.

We conducted an experiment to see the applicability of using actual pitch vs time representation in notation form to find out whether participants who haven’t studied music thoroughly understand these visualizations any better than written notation which doesn’t contain graphical information. 

\begin{figure}[H]
\centering
		\includegraphics[scale=0.2]{figures/chp3/5notesmelo.png}
	\caption{Examples of melographs analyzed for 5 different articulation types.}
	\label{fig:5samemelo}
\end{figure}


\begin{figure}[H]	
\centering
		\includegraphics[scale=0.2]{figures/chp3/examplequestion.png}
	\caption{Sample question in the quiz task}
	\label{fig:samplequestionnotation}
\end{figure}

\subsection{Method}
We conduct an identification and recognition task with 24 participants, of mixed genders, all of whom were music enthusiasts. 
 All of them took a 2 minute tutorial to learn both types of notation systems. The next day, they were given a quiz of 16 questions and their answers were weighted and scored. We create a total of 16 questions with 4 questions belonging to 4 levels of hardness each. An example of the type of questions asked is illustrated in \ref{fig:samplequestionnotation}
Fig \ref{fig:5samemelo} illustrates five sung renditions of a musical phrase in melograph notation. These would be written in the same way in the Bhatkhande style. 

Out of the 4 difficulty levels, while scoring, each level was assigned weight, easiest getting a weight 1 and hardest was weighted by 4. 
There were 2 question types

 - Matching Audio to correct notation(graphical/bhatkhande) from four choices
 
 - Matching Notation to correct audio from four choices


\subsection{Results}

\begin{figure}
\centering
\begin{tabular}{cc}
		\includegraphics[scale=0.4]{figures/chp3/graphnotres1.png} &
		\includegraphics[scale=0.4]{figures/chp3/graphnotres2.png} \\
\end{tabular}	
	\caption{Answers (a) and Weighted answers (b)}
	\label{fig:results1}
\end{figure}


\begin{table}[H]
\centering
\caption{Results of Notation Study}
\label{table:notationresults}
\begin{tabular}{llll}
Comparing Means {[} t-test assuming equal variances (homoscedastic) {]} &             &                              &          \\
\textbf{VAR}                                                                     & \textbf{Sample size }& \textbf{Mean                         }& \textbf{Variance }\\
18                                                                      & 17          & 14.14706                     & 30.61765 \\
14.5                                                                    & 17          & 13.23529                     & 16.97243 \\
                                                                        &             &                              &          \\
\textbf{Summary                                                                 }&             &                              &          \\
Degrees Of Freedom                                                      & 32          & Hypothesized Mean Difference & 0.       \\
Test Statistics                                                         & 0.54494     & Pooled Variance              & 23.79504 \\
Two-tailed distribution                                                 &             &                              &          \\
p-level                                                                 & 0.58957     & t Critical Value (5\%)       & 2.03693  \\
One-tailed distribution                                                 &             &                              &          \\
p-level                                                                 & 0.29479     & t Critical Value (5\%)       & 1.69389  \\
G-criterion                                                             &             &                              &          \\
Test Statistics                                                         & 0.06078     & p-level                      & 0.12668  \\
Critical Value (5\%)                                                    & 0.162       &                              &          \\
                                                                        &             &                              &          \\
Test Statistics                                                         & 0.54494     & p-level                      & 0.41008  \\
\end{tabular}
\end{table}

We find there to be no significant difference between groups for understanding any notation type better. We performed a t test to find the difference between groups, and get a p value of 0.41. Even though graphical notation may be more intuitive, it does not mean that the melograph is a directly usable, very intuitive form of notation.

Given as how participants of this study could not directly relate correspondence between mental imagery and melograph plotting, we perform sound tracing study on phrases in Hindustani music to see whether this effect is due to absence of signal profiling. Mental conceptualizations of space may be entirely different from the signal manifestations of it. 



\section{Experiment 2: Sound Tracing}
Computer controlled interfaces and instruments have been explored in the area of sound creation and manipulation. Such interfaces allow a user not only to control musical parameters but also to express through them. New interfaces can dissociate the expressive action from the sound in a variety of ways, making the mapping between action and sound complex and indeterminate \cite{jorda2005digital}. 

Research in new musical interfaces predominantly concentrates on generating idiosyncratic grammars for novel instruments, although little has been done to create interfaces which accomodate existing grammars that musicians are already accustomed to. One of the focuses has been on the creation of new textures, but there has been limited exploration in mapping novel ways of controling music with existing musical grammars.
This limits users from having virtuosic control over the instrument  \cite{dobrian2006nime}. There are also devices that map data gathered to an existing formalized scheme of music \cite{cadoz2000gesture}.

The first generative theory of tonal music \cite{lerdahl1983generative} formalized some concepts of composition and presented hierarchical systems that shape musical intuitions. \cite{cope2004virtual} \cite{cope2005computer} explain some methods of generating computer music in the Western classical style. These methods also learn and generate signature motifs used by particular composers. However, they cannot be used in Hindustani Classical Music (HCM), because the parallels between the concepts of western tonal theory and HCM are limited to tuning and modal constructs. 

The probabilistic generation of HCM was first proposed in \cite{bcdeva}.  Music generation in \cite{ragamatic} explores the probabilistic modeling of raga grammars. Similar strategies have been used in music information retrieval systems for HCM \cite{krishna2011identification} \cite{ras2010advances} \cite{bellur2012knowledge} to help identify ragas as well. Although the methods to learn raga grammars are largely based on machine learning techniques \cite{chordia}, generativity in Hindustani music has not been studied explicitly. Strategies for creating valid phrases in HCM have not been investigated either, because extracting information from live recording and mapping it to analytical concepts from music theory were found challenging. Hence there is an opportunity for new interfaces to create Hindustani music, as computer-aided compositional tools. 

Mapping motion to music is a known pedagogical technique used both in Western and Indian musical systems. Input device technology that considers human motion has also been explored in the form of gestural and motion capture based systems \cite{itrace}. More recent studies have focused on the relationships between sounds and natural movements through tracings of sound objects \cite{Jensenius} \cite{Godoy}. Data from human motion has also been mapped to a scheme of generative music \cite{dancetrace} while \cite{itrace} focuses on free musical textures in improvisation. Sound-tracing studies to distinguish between trained and untrained musicians have also been conducted in \cite{kussner1} to quantify shapes as natural visual representations. 

Due to the language-like nature of phrase mappings in HCM, phrase level grammars are a known compositional technique \cite{rowell} \cite{varna}. We explore the application of these grammars through the medium of visual representations and propose a system for computer-aided composition which dynamically generates valid phrases in ragas from a tracing. Our goal is to use sound-tracings as an interface to generate raga phrases. In the next section we describe our initial data collection process to learn a model for melodic mapping. The interface design is then discussed in section 3.
Links between sound and gesture could be understood within the framework of embodied cognition, meaning that perception and cognition of sound is based on both neurocognitive constraints and massive ecological experience of sound-gesture relationship, including explicit, as well as more implicit, knowledge of causality, resonant features of entities, and sound-producing gestures. Here we hope to see strong correlations between people’s natural movements accompanying the auditory perception of musical phrase.

Formal theory of melody in Indian music appears in many texts [18] [17]. All possible melodic phrases are divided into four categories or varnas: 
\begin{enumerate}
\item Aarohi / Ascending (A)- global ascending contour for pitches in the melody 
\item 	Avarohi / Descending (D)global descending contour for pitches in the melody 
\item 	Sthyayi / Stationary (S)hover around a stationary point in the melodic frame 
\item 	Sanchari / Random (R)do not fit any of the above three categories 
\end{enumerate}

To begin with, we conduct an experiment to collect sound tracings as a response to the aforementioned phrase categories and explore their classifiability to build a corpus. 

\subsubsection{Stimuli Design}
We create a set of 64 stimuli in various combinations. The duration of each stimulus is between 4 and 6 seconds. All stimuli were sung by one singer. There were three independent variables as follows, 
\begin{enumerate}
\item Phrase Type: This includes the four classes for melodic categories as described above. 
\item Articulation style: These are divided into two types: Gamak and Flat notes. Gamak notes are with continuous slides between two notes. Flat notes do not have this kind of representation. 
\item Thaat group: We divide the ten thaats in HCM into four groups on the basis of their affinity and general nature. 
\end{enumerate}


\subsection{Experimental Setup}
In the first part of the study, we obtain data for each of the four phrase categories from 28 participants (median age = 24.96) mixed across both genders. The stimulus set was divided into eight playlists. We analyze the tracings drawn by 28 participants and compare them according to the phrase types used. We find strong correlation between natural movement accompanying auditory perception of musical phrase and some features of the music itself: pitch positions, dynamic stresses and accents. Participants were asked to trace 32 musical phrases on a WACOM Bamboo digital tablet. 
After obtaining a dataset containing 32 × 28 tracings equally distributed over all classes, we approximate our tracings as described in section 3.1 and group them into melodic phrase categories using a K Nearest Neighbor(KNN) classifier. The classification results as discussed in the section 3.2 as a part of the interface design. In Fig. 2, we plot a characteristic example of the first three phrase categories along with all the user tracings in the second column. 

\subsection{Qualitative Results}
Out of 29 participants in this study, 24 reported having traced pitch data vs time. Despite this, we find significantly different visual models for each participant. Musically trained participants, or the ones who are highly inclined to music tend to be more consistent with phrase envelopes than musically untrained participants. Overall, everyone was able to pick up more nuanced pitch profiles and with time, and their sketches became more and more detailed as the experiment went forwards.

\paragraph{Classifiable data across 4 categories}
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{figures/chp3/fourPhraseCategories.png}
\caption{Four Phrase Categories}
\label{fig:fourCategories}
\end{figure}
We expected that participants would present sketches that would be discriminable into four categories. In the above figure, we can see some example representations of four categories of phrases into four different types.  

\paragraph{Ascending Phrase Bias}
\begin{figure}[H]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.3]{figures/chp3/ascendingPhrases.png} &
\includegraphics[scale=0.3]{figures/chp3/ascendingPhraseBias.png} \\
\end{tabular}
\caption{Ascending Phrase Envelope}
\label{fig:ascendingphrase}
\end{figure}
As seen in figure \ref{fig:ascendingphrase}, we found that not only were ascending phrases represented by ascending sketches, but many other types of phrases were also represented using an ascending sketch profile. This changed for many participants later. We found that untrained participants have a natural inclination to draw all phrases as ascending phrases. This could be because there is a tendency to represent time continuum as an ascending entity. We found that there is a clear difference between trained and untrained participants in music. Although everyone reports that they tried to draw pitch, some participants reported confusion and a tendency to oscillate between amplitude and pitch. 


\paragraph{Articulation Representation}
 \begin{figure}[H]
\centering
\includegraphics[scale=0.3]{figures/chp3/flatGamak.png}
\caption{Differences in Articulation Types}
\label{fig:flatgamak}
\end{figure}
There were differences between different kinds of articulation - gamaks, which are vocally heavy and oscillating were more often represented with wavering lines as opposed to straight lines. This different was most visible in the 'sthyayi' phrase category.

 
\paragraph{Reproducibility of sound tracings for repeated stimuli}
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{figures/chp3/repeatTracingExamples.png}
\caption{Differences in Repetitions of Tracings}
\label{fig:repeats}
\end{figure}
At the end of the test, we had 8 stimuli per participant randomly repeated. This was to see if the participants were consistent in their drawings. If the sketch profiles changed after repetition, it would mean that the results are somewhat random. We found there to be a high level of consistency amongst musically trained participants.


\paragraph{Pitch verticality in tracings}
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{figures/chp3/pitchVerticalitySignal.png}
\caption{Vertical Representations of Pitch}
\label{fig:pitchvert}
\end{figure}
Pitch profiles were matched vertically for some participants more than others. Everyone thought that the sketch should begin to their left and end on the right of the tablet without this being an instruction.


\paragraph{Ambiguity in referencing base notes}

Relative scaling of perceived pitch is most relevant only in a local sense. The height represented for a pitch that is very high need not be the height represented for the same pitch the next time - but this difference is accounted for by the local reference notes. This means that in a stationary phrase, even a small difference is more significant than a large difference in an extremely variant phrase.

\paragraph{Special Cases}
\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{figures/chp3/child.png}
\caption{Sketches of a child}
\label{fig:child}
\end{figure}
In figure \ref{fig:child}, we can see the sketching on the tablet of an 8 year old participant. Her idea of a left to right sketch was not fixed, and she drew over her sketch quite a number of times. One reason this was consistent could be the lack of visual feedback on the tablet. While sketching, you could not see what you drew.

\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{figures/chp3/harmonium.png}
\caption{Sketches of a Harmonium Player}
\label{fig:harmonium}
\end{figure}
We had another outlier as a musician who played harmonium. Their pitch visualizations were closely mapped to the keys on the harmonium, and they interpreted the pitches and drew them out where the keys were on the harmonium.


\subsection{Interface Design}
TrAP interprets the users’ tracings and generates a valid musical phrase based on a selection of ragas. TrAP relies on various rules specific to ragas while generating a phrase. The components of TrAP are shown in the schematic diagram of Fig. 3. 

\begin{figure}[H]	
\centering
		\includegraphics[scale=0.6]{figures/chp3/Flow}
	\caption{Structure of TrAP}
	\label{fig:Structure}
\end{figure}


\subsection{Tracing Approximation}
Given a tracing $T ={(x,y)}$, where $x$ is the time axis and $y$ is the pitch axis, we normalize its length, and center the $y$ values across its mean. We now interpolate the normalized tracing using a cubic spline function with 8 knots. This approximation is required to remove aberrations and discontinuity from the tracing, thereby capturing the global contour of the tracing. 

\begin{figure}[H]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.3]{figures/chp3/ap.png} & 
\includegraphics[scale=0.3]{figures/chp3/au.png} \\
(a) & (b) \\
\includegraphics[scale=0.3]{figures/chp3/dp.png} &
\includegraphics[scale=0.3]{figures/chp3/du.png} \\
(c) & (d) \\
\includegraphics[scale=0.3]{figures/chp3/sp.png} &
\includegraphics[scale=0.3]{figures/chp3/su.png} \\
(e) & (f) \\
\end{tabular}
\caption{Pitch data and corresponding user tracings for Ascending(a,b), Descending(c,d) and Stationary(c,d) phrases}
\end{figure}


\subsection{Phrase Classification}
Using the approximated tracings as raw features for the system, we trained a K-nearest neighbour classifier (KNN) with cityblock distance metric. With the top K results, we selected the mode to be the output class label. 

\begin{figure}[H]
\centering
\includegraphics[scale=0.8]{figures/chp2/ConfMat.png}
\caption{Confusion Matrix for Phrase Classification}
\label{fig:confmat}
\end{figure}
We performed a 10-fold randomized cross validation to check our system’s performance. Class accuracies of 81\%, 63\%, 41\%, 28\% for ascending, descending, stationary and random phrase types were obtained with an overall accuracy of 54\%. We observed better performance for the first three classes as they tend to have a fixed global contour, whereas the random class (sanchari) does not \ref{fig:confmat}. 

\begin{algorithm}
\KwData{\;
$L \rightarrow$ Subtracing label \;$N \rightarrow$ Notes per subtracing \;
$b \rightarrow$ Previous phrase last note \;
${S}_n \rightarrow n-$Note sets \;
${T}_n \rightarrow n-$Transition sets \;
$F \rightarrow$ Forbidden consecutive notes \;
${R} \rightarrow$ Acceptable thresholds per class \;
}
\KwResult{ $P \rightarrow$ Final Phrase}
initialization\;
$s \rightarrow$ pick initial set from ${S}_n$ \;
$n \rightarrow 0$ \;
$P \rightarrow b$ append $P$\;
\While{$n < N$}{
	$r \rightarrow$ pick random note from set $s$ \;

	\If{ last note $P$ and $r$ in $F$ }{
		continue \;
	}	
	
	$P' \rightarrow P$ append $r$ \;
	
	\If{mean(derivative($P'$)) in range $R_{L}$}{
		$n = n + 1$ \;
	}
	
	$s \rightarrow$ pick random set number from $T_n(r)$ \;
}
\caption{Phrase generation algorithm}
\label{algo1}
\end{algorithm}



\subsection{Tracing Segmentation}
Once a tracing is drawn, it is split into equally-spaced divisions based on a user parameter. After spliting, each sub tracing is classified by the KNN search described above and assigned a class label. The user parameter is to specify the resolution to which information can be utilized from the tracing. The granularity can be exploited to generate a phrase closer to the tracing by selecting a high number of divisions. The subtracings obtained are interpolated to the desired feature vector length and classified using the K N N classifier described above. 

\subsection{Phrase Generation}
Given the subtracings and their respective class labels, we generate subphrases for each class label iteratively. We concatenate them all to get the final phrase in the end. All subphrases generated are built using the grammar specified by the raga explained in section 3.5. Algorithm 1 presents the details of the system component. The raga grammar rules are stored as different sets of notes, transition tables for moving from one set to another and a list of forbidden transitions. All random assignments are based on distribution built from domain knowledge in raga formation in relative scaling. We initially find out the relative occurence of a note after a subjective evaluation which is then quantifed and given a muchness score to produce realistic phrases. 

\begin{figure}[H]	
\centering
		\includegraphics[scale=0.7]{figures/chp3/phrase1}
	\caption{A sample phrase generated by TrAP in Raga Yaman}
	\label{fig:Structure}
\end{figure}

The classification of an intermediate phrase during its construction is based on the mean of the first-order derivative (referred as μd) of MIDI sequence values. The μd has bounds for each class of phrase, which serve as a constraint during the iterative process of generation. Since the chosen MIDI sequence values are discrete and a single increment represents a half step, it allows us to objectively define the class thresholds over μd and constrain them as needed. Fig. 4 shows an example of three sample phrases generated by the system in raga Yaman. 

\begin{figure}[H]
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.2]{figures/chp3/userdraw.png} &
\includegraphics[scale=0.2]{figures/chp3/userdrawp1.png} \\
(a) & (b) \\
\includegraphics[scale=0.2]{figures/chp3/userdrawp2.png} &
\includegraphics[scale=0.2]{figures/chp3/userdrawp3.png} \\
(c) & (d) \\
\end{tabular}
\caption{Sample tracing (a) and three TrAP generated melodic phrases in raga Yaman shown in (b), (c), (d) }
\end{figure}

\subsection{Raga Grammar}
In this section we explain a framework for adhering to the raga structure to create valid phrases. First we determine the intonation and note positions of the constituents notes of a raga. We describe some traditional categories of defining a raga. At any point of time in exposition, the constituent notes, the heavily used notes for each raga, and some characteristic phrases, largely determine the melodic movement. We can use this strategy for generating random sequences of valid phrases in HCM if we model the following parameters: 

\begin{enumerate}

\item Vadi This is the most important note of a raga. It is the note towards which the melody mostly gravitates. 
\item Samvadi This is the second most important note of the raga. 
\item Anuvadi This is a note that is used optionally in rendition, to beautify the raga. 
\item Bahutva This literally means muchness or copious use. There are two types of Bahutva: Abhyas Mulak and Alanghan Mulak, which are two degrees of muchness. 
\item Aplatva This literally means scant or non-copious use. There are two types of Alpatva: Langhan Mulak (to be used in transition) and Abhyas Mulak (almost not sung at all). 
\item Nyas This is the cadential or the final tone to be used at the completion of phrases. 
\item Poorvanga / Uttaranga Vadi This is determined by whether the vadi or the tenor note of a raga is in the Poorvanga (first half ), or the Uttaranga (second half ) of the singable octave. This will help determine the hover point of the phrases, and there would be a higher chance to select phrases from this part of the octave, or for the phrases to move in this part of the octave. 
\item Characteristic Phrases These are phrases that occur very often in the rendition of a raga. 
We do this by employing the following strategies: 
\begin{itemize}

\item Picking an accurate pitch class set for the chosen raga from the singable octave from the corresponding midi notes for a raga. 
\item The notes chosen must be around each other to ensure that phrases have continuity. We enforce this by dividing the octave into four parts from which pitches are chosen to restrain the amount of jumps in the melody. A random set is picked to start with each time. There are overlapping notes between adjacent sets, that are pivot points from which the melody can transition. In Table 2, we see the four sets that divide the two octaves that are sung and used, and the respective transition sets, along with their assigned muchness scores based on the above characteristics. 
\item Assigning each note a measure of muchness based on the above 8 criteria between level 1 and 5. A nonuniform probability distribution for picking notes from each set which follows the Alpatva / Bahutva paradigm in HCM as explained above, where some notes are sung less frequently than others. 
\item Forbidding the note transitions that are disallowed in the raga structure: The example in Fig. 3 is that of a phrase generated in raga yaman. In this raga, it is not possible to go from a Ti to a Re, and we enforce rules like this for note transitions, allowing the phrases to remain valid. 

\end{itemize}
\end{enumerate} 	

\begin{table}
\centering
\caption{Pitch Sets in different Ragas}
%\scalebox{0.8}{
\begin{tabular}{|p{2cm}|p{2cm}|p{2cm}|} \hline
Raga Name&Notes Contained (A)&Notes Contained (D)\\ \hline
Yaman&Ti Re Mi Fi Sol La Ti Do&Do Ti La Sol Fi Mi Re Do\\ \hline
Bhupali&Do Re Mi Sol La Do&Do La Sol Mi Re Do\\ \hline
Bageshri&Te Do Me Fa La Te Do&Do Te La Sol Fa Me Re Do\\ \hline
\end{tabular}
%}
\label{tab:Pitch}
\end{table}

\begin{table}[H]
\centering
\begin{tabular}{lllll}
Note & MIDI Value & Set Numbers & P(Transition) & Muchness \\
Pa   & 55         & 1           & 1, 2          & 1        \\
Dha  & 57         & 1           & 1             & 1        \\
Ni   & 59         & 1, 2        & 2, 1          & 3        \\
Sa   & 60         & 1, 2        & 1, 2, 3       & 3        \\
Re   & 62         & 1, 2        & 2, 3, 4       & 2        \\
Ga   & 64         & 2           & 2, 3, 1, 4    & 5        \\
Ma   & 66         & 2, 3        & 2, 3          & 2        \\
Pa   & 67         & 2, 3        & 2, 3, 4       & 3        \\
Dha  & 69         & 3           & 3, 2          & 1        \\
Ni   & 71         & 3, 4        & 3, 4, 2, 1    & 4        \\
Sa   & 72         & 3, 4        & 3, 4, 2       & 3        \\
Re   & 74         & 4           & 4, 3          & 2        \\
Ga   & 75         & 4           & 4, 3          & 3        \\
Ma   & 77         & 4           & 4             & 2        \\
Pa   & 78         & 4           & 4             &         
\end{tabular}
\end{table}






\section{Conclusions}
We presented here an interface, TrAP to generate valid melodic phrases in different ragas. The interface has two user-defined parameters to control granularity and length of the final phrase. The system allows the user to also determine the contour of the melodic phrases. Transition tables and pitch class sets can be populated with several other ragas enabling the user to express the tracings in different ragas. Future work can include rhythmic variation and enabling higher order compositional structures through this schema.

Theoretically, the parametric information of different ragas can be added to this tool for getting output phrases in them. These ragas therefore need to have an ontology. The spatialization of these raga categories is graphed and mapped in the chapter 4. We explain a method to construct such an ontology and use it for establishing a notion of proximity between raga categories.

