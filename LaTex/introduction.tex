\section{Introduction} 
\label{sec:intro}

Music and movement have always been tied together in performance and appreciation. In stage performances, musicians and even instrumentalists gesture or sway their bodies with the music; dance is simply motion corresponding to music; and the elaboration of musical motion is manifold.
The discourse about multi-modality in musical practise is not new to musical research. Learning and performance in music have been known to involve spatial and visual components, that express themselves in various different ways. 

Western notation finds its roots in \textit{neumes}, which were graphical note markers, that helped people remember the compositions of the texts. This gradually moved to more and more elaborate symbols, giving rise to the notation system we now use. Even this notation system has had a graphical revival in the modern times, with more and more composers using graphical symbols for concrete representation of notes. Graphical representations of music not only leave more room for interpretation than notation in full detail, but it is also a very different type of interpretative freedom. Another obvious way in which music and spatial tasks are related is the practise of conducting. Conductors, with their elaborate body gestures express and direct the orchestra to all have one voice. Some of the gestures of the conductor are codified, but there is also a lot of freedom to make richly expressive, interpretative gestures.


The development of Indian music has its roots in dance and theatre, which were always delivered with music. Even today, improvised performances of Hindustani music are always accompanied by elaborate hand gestures. Firstly, these gestures serve as representations for the music itself. Secondly, they help performers formalize their mental representations, which are spatial to begin with. Thirdly, these gestures engage the audience who sometimes repeat them and better understand the music performed. 

Vocal music is the dominant form in North Indian music. Instrumentalists are often advised to 'play like a vocalist would sing', and the idioms of the most instruments are often drawn from their vocal counterparts. Musicians playing other instruments are often also made to study singing in the beginning, and are expected to know the finer ways of handling raga idioms vocally. These idioms too, are very often taught using hand gestures that are not representative of the instrument, but using general spatial metaphors just as vocalists are taught. Vocalists also have their hands free, making it possible for us to analyse these gestures easily. It is for this reason that we focus on vocalists and their gestures for the purpose of this study.

Most performers who study Indian music grow up imitating their teachers' phrases, along with the teachers' hand gestures. Gesturing of this type, thus, seems very natural to everyone who has grown up in this system. Many performers have not even thought about these gestures as being an important feature in music, and describe them as natural actions. What makes these gestures natural, and what are further nuances of this practise of gesturing?

In this thesis, I explore how these gestures are related to music - are they strongly melodic gestures or rhythmic? What is the topology of the space through which these melodies seem to be moving? Are there musical 'objects', that a performer can grasp, and most importantly - what is the cognitive construction of these gestures? If the gesturing is grounded in a mental construction of shape in space, then can we safely get these representations out in other ways, such as drawing music, for example. First I explain the background against which this research is laid.


\section{Thinking about Music and shape}
In western music, there have been some attempts to provide empirical demonstrations of the kinematic correlates of Western art music. These largely start with three German researchers: Sievers, Becking and Truslit, whose writings about this date from 1924 to 1964 \cite{reppSieversEtc}. The first attempt to systematically use kinesthetic representation in not only music but also to literary works was called \textit{Schallanalyse}, and was developed by Eduard Sievers. He distinguished two classes of curves - general curves or "Becking curves" and specific or filler curves (\textit{Taktfüllcurven}), which described few types of musical movement corresponding to dynamic expression and voice type. Special curves reflected the metric and sonic properties of spoken text or music.

Becking and Truslit distinguish three basic types of movement curves: "open", "closed" and "winding". These curves are not conducting movements - they are rather supposed to be carried out with outstretched hands an are a mens of portraying dynamics in space, with speed of movement and consequent relative tension governed by the curvature of the motion path.

Clynes in 1977 \cite{clynes} developed a notion of essentic forms, dynamic shapes that characterize basic emotions. He developed an apparatus called a \textit{sentograph}, which would capture finger pressure in two directions while listening to music. It was said that subjects produced different 'sentographs' while listening to music that portrayed different feelings. He developed a computer program that enabled him to play back music with different agogic and dynamic patterns. Todd, later in 1992 is concerned with the motion of the whole body rather than just limbs or fingers. He finds evidence for motion and music from the ventromedial and lateral systems, controlling posture and motion. His study of motoric instantiations in the 'expressive body sway' have been important.

\begin{figure}
\centering
\includegraphics[height=4cm]{figures/intro/sievers}
\caption{Examples of movement curves used by Sievers}
\label{fig:sievers}
\end{figure}

\begin{quote}
The listener's body thus acts like a \textit{transducer}, a kind of filter for the often impulse-like coding of musical movement.
\end{quote}

\subsection{Gestures in other musical styles}

Performative gestures are arguably important to listening to all genres of music.\cite{seeing}. In the analysis of B. B. King's music in it was found that some gestures have the effect of drawing the listeners’ attention to local aspects of music, specifically to the nuanced treatment of individual notes, and away from larger-scale musical structure. But his body movements also reflect large-scale structure, becoming generally more pronounced as the performance moves to the point of climax.
Flamenco singers are also found to heighten the rhetorical impact of their performance with dramatic movements of the hands, arms, and eyes. Singers of Peking Opera adopt body gestures depending on several character types. Systems of chironomy are a part of several chant traditions including Yemenite Torah \cite{yemenite} recitation. 
\\

There have been several studies focusing on gesture recognition and analysis of conductor gestures. Western musicians who perform in ensembles do so with the help of conductor gestures. A conductor not only gives the performers a signal to start and end on time, keeping the cue for everybody, but is also responsible for providing an interpretation of the score. This interpretation is performed in the mode of both hand movements as well as facial expressions and so on. 
Some work has been proposed towards ‘pragmatics’ and ‘semiotics’ of conducting. 
A study by Boyes-Braem \& Bräm (2000) \cite{boyesbram} examined ‘expressive’ gestures made by conductors, using an expert informant to code such gestures for meaning and comparing the form of conductor gesture to standardized sign-language. Similarly, Poggi (2002) \cite{poggi2002} undertook a study of conductors’ facial expressions, with the aim of developing a lexicon, again based on a single informant. Parton (2009) \cite{parton2009} undertook a study of conductor gestures according to McNeill’s continua \cite{mcneillhandandmind}, suggesting that conductor gestures show some consonance with gestures produced alongside speech, and with normative human gestures. These studies are generally limited to qualitative micro-analysis of synchronic examples of conductor gesture, or to particular features across a set of gestures. These studies, however, clearly situate conductor gesture within a broader field of the study of interaction and communicative practices. 
\\
In Hindustani music, the main vocalist sometimes acts as a conductor as well, directing the ensemble and other musicians and communicating the performance plan to them on the fly. 

\subsection{Conceptual Metaphors and Gesture} 
\label{sec:conceptmeta}

A more recent discourse on music and multi-modality starts with the application of Lakoff and Johnson's theory of linguistic metaphors. The premise of multi-modal metaphors is that linguistic conceptualization takes place by associating concepts from different modalities together. In this way, abstract concepts also get represented by concrete spatial or other metaphors. An oft cited example of this is the common phrases: 'to get an idea \textit{across}'
'\textit{long} time'
'what's \textit{in }this article?'.

Conceptual metaphor theory (Lakoff \& Johnson, 1980, 1999) \cite{lakoff1980} \cite{lakoff1999} first suggested that linguistic metaphors like these show that many abstract ideas are structured as metaphors for spatial concepts. 
This spatial relationship of linguistic metaphor also might extend to the link between language and gesture. In David MacNeill's 1992 book, 'Hand and Mind: What gestures reveal about thought', this relationship is explored. This book talks about the nature of co-speech gestures and idiosyncratic movements of the body that associate with speech. It is suggested that the microevolution of an utterance may in turn epitomize the macroevolution of linguistic system from primitive gestures. 

This book also explores a typology of gestures, such as iconics, metaphorics, diectics and beat gestures. Beat gestures are those where hands display two-phase movement in time with speech, to indicate emphasis in some words over others. Diectics are pointing gestures that indicate the position or location of an object. Iconic gestures are specific gestures which are coded within a culture or community to mean something. Metaphorics is the branch that deals with gestures that serve as metaphors for other abstract things in language. A good example of this is the 'cup-of-meaning' hand shape, where the speaker refers to an abstract idea with a cup shaped gesture, which then changes shape when the idea is refuted or changed. Data from several studies related to metaphorics in gestures have suggested that speakers in demanding communicative situations - especially ones where abstract ideas were needed to be expressed – conceptualize those ideas in space (Enfield, 2005) (Nuñez, 2004; Sweetser, 1998). These metaphorical gestures may be helping in the conceptualization of abstract concepts by grounding them in space. 


These co-speech gestures have observable 'referents', even if they are metaphorical or abstract. Music, however is semantically void and cannot itself have concrete references or referents. The abstract nature of music makes it challenging to analyse gestures by putting them into any of these possible typographies. It also makes it hard to analyse what the gestural associations mean. Do they represent cognitive schemas and back-end multi-modal structures or are they an epiphenomenon, reflecting habits formed during training.  

Early thinking of musicologists about gesture has emerged from this discourse about metaphorics in gesture studies. The empirical and other evidence for these comes from multidisciplinary sources.

 \begin{quote}
 In fact, the diverse use of the word "gesture" [in music] can be seen as a testimony to the great importance that people attach to making some kind of a recognizable action or movement. - Godoy and Leman(Musical Gesture) \cite{lemangodoy}
 \end{quote}


\subsection{Musical Gesture and Semiotics}

Leman was arguably the first to formalize a theory of embodiment in music cognition. His central suggestion is that the body is a mediator between musical experiences and the physical environment, thus translating between physical properties of music, the acoustic signals, and our musical thoughts, values and intentions. In his book \cite{leman2008}, he integrates three perspectives: third-person (measureable physical motion), second-person (bonding, empathy, communication, and interaction), and first-person (phenomenal observation). Gestures in music operate on all of these levels for function as well as a matter of cognitive construction. Incidentally, analysis of gesturing also operates on these different levels. Third person level of analysis would include motion tracking and video recording, second person would be audience interaction with performers and enhancing their understanding of the music by embodying the gestural aspect of the communication, and lastly, the individual mental schema that help generate these gestures.
 
In semiotics, musical gesture means more than just physical body gestures. Musical gestures in and of of them selves could be translations of physical phenomena into musical categories. In the same way as triplets in rhythm have come to mean royalty, or music getting gradually softer and slower represents the distance of the listener from the musical object, these semiotic properties stand for a lot in cultural contexts. David Lidov, \cite{lidov1987} has developed a view of the semantic content of melody without text, emphasizing gestural representation. In this way of analysis, gesture is interpreted as a semiotic phenomenon, and musical gestures or nuances in music have signification beyond being abstract ideas. 

Semiotic associations of gesture and music are also explored in Music and Gesture \cite{gritten2006music} and New Perspectives on Music and Gesture \cite{gritten2011new}. These books contain multiple perspectives on music and motion written by several authors working in the field. Semiotic meaning of musical gesture, as well as the embodied content of musical gesture is explored in these books.


\subsection{Neurological evidence for music spatialization}

There are some studies suggesting neurological evidence of the visual, auditory and somatosensory information getting combined into a spatial representation (\cite{spence1997audiovisual}; \cite{stein1995neural}). Neural evidence for spatial cortex activation at the time of being given music related tasks, is also supplemented. \cite{platel} is a study into the structural components of musical perception. In some studies, it is shown that the right auditory cortex might have the brain regions linked to pitch mappings \cite{johnsrude2000functional}.

Many of these studies suggest that the frequency of attributing spatial relationships to pitch is exemplified in regarding pitch as up or down. \cite{rusconi2006spatial} mentions a study suggesting that pitch high / low serve as spatial metaphors for hearing, but this is not true for all languages. In Indonesian languages, pitch is talked of as being big or small. The resulting musical metaphors used to represent this conceptual change also correspond to the feeling of big / small rather than high / low. 

Pitch is described with words for up / down in several languages. Zbikowski, in his book Conceptualizing Music: Cognitive Structure, Theory and Analysis \cite{zbikowski}, investigates this tendency to look at pitch as a vertical object. Some other studies corroborate this finding \cite{rusconi2006spatial}. Apart from verticality, other cross-domain metaphors for pitch are also investigated by empirical studies \cite{eitan2010}. Analysing pitch in terms of having only high and low parameters happens to be ethnocentric, as many other languages use many different kinds of spatial metaphors. Also, it is important to investigate whether this high / low represents the high / low of metaphorical quantities in general. This has been carried out in a study \cite{eitan2006music}. This study further suggests not only that verticality is not the only parameter affected by pitch contours, but all three spacial axes are represented in the mental representations of pitch.

Another important question asked in this study is whether our representations of high and low corroborate with other instances of using High and low as metaphors for other abstract quantities. This would mean that other metaphors for 'high and low', should corroborate with use of high and low pitch, which is not found to be true. This means that conceptual pitch height is a feature of pitch that doesn't correspond directly to other conceptual height metaphors.


\section{Sound Objects and Action-Sound}
Our understanding of action and sound is described by psychologists as "intermodal feature binding awareness" \cite{multisensory}. This is what enables us to know that the visible vibration of a string, and the sound of the twang are emergent properties of the same action.
\begin{quote}
When presented with two stimuli, one auditory and the other visual, an observer can perceive them either as referring to the same unitary audiovisual event or as referring to two separate unimodal events .... There appear to be specific mechanisms in the human perceptual system involved in the binding of spatially and temporally aligned sensory stimuli.
\end{quote} (Vatakis and Spence, 2007, 744, 754; quoted by O’Callaghan, 2014, ms p. 8)

It is debated that this process could just be an instance of unitization. Seeing instances of more than one feature occurring together (albeit belonging to different modalities), helps us 'chunk' multimodal features together, and perceive them as being a part of the same action. This raises several questions - what does this unitization mean for cases where action sound couplings are synthetic? How does it help us couple action and sound in cases where the action is not a sound producing action?

Godøy, (2003) \cite{godoy2003} describes a theory of motor-mimesis, as a mechanism by which we translate musical sound into visual images by taking a simulation of sound producing actions – of both singular sounds, as well as complex musical phrases and textures, helping us to re-code and store musical sounds. Jensenius, (2007) \cite{jensenius2007action} provides methods of analysis of musical gestures in western music, using different methods such as motion capture, sound tracing, and others. These are synthetic methods of capturing the conceptual schemas through which music is interpreted. 

The underlying concept of 'shape' in music has also been mentioned in several musicological studies. This underlying notion is discussed as dynamic shape of phrase units, or pitch height described in time. Pierre Schaeffer, who was the pioneer of the work Musique Concrète \cite{schaeffer1967musique}, focused on analysing the sonic event, together with all of its qualities. Traditional musicology has focused much more on melody, harmony and rhythm, but for Schaeffer, the inclusion of dynamic, texture and timber are as important as the musicologically salient, and more quantifiable qualities of pitch and rhythm. Schaeffer also focussed on how non musical sounds become musical via repetition. Schaeffer proposes a sound-object - Objet Sonore \cite{shaeffer1999solfege}, which is a sonic event permeating direct classification only by musicological constraints. He also described a typology of these sonic objects, and methods for analysing them.

For Godoy, sonic features all vary and evolve in time and space, which makes it possible for sonic events to be thought of as gesture.  He also points out that Schaeffer’s system of categorizing sound features—the typology and morphology of musical segments is largely based on gestural metaphors and that the three broad typologies put forward by Schaeffer — impulsive, sustained and iterative — can be directly linked to the physical gestures necessary to produce such sounds. 

In Godøy’s (2006, p. 149) words: 
“This means that from continuous listening and continuous sound-tracing, we actually recode musical sound into multimodal gestural-sonorous images based on biomechanical constraints (what we imagine our bodies can do), hence into images that also have visual (kinematic) and motor (effort, proprioceptive, etc.) components.” 

For Godoy, a gestural sonorous object isn't one that follows through the entire duration of music, but stays with just one phrase or musical segment. To gain information about the inaccessible features of music, the analysis of these internalized qualities of music can be obtained by drawing or imagining to draw or trace the music as it is going on. This approach opens up new methods of analysis of music that go beyond acoustic features. 

This thesis, in conjunction with Schaeffer's analysis of sound objects has been used in Jensenius \cite{jensenius2007action} to carry out sound-tracing based experiments, that try to capture the gestures related to sounds (but not musical sounds). I explore the same shape capturing paradigm to relate it to the physical motion accompanying Hindustani classical music in Chapter 3.


\section{Tonal Pitch Space}
In his book titled Tonal Pitch Space, Fred Lerdahl explains several ways of looking at the topology of musical organization. The musical proximity of several parameters, such as chord patterns, tonal functions and so on, is translated into geometric proximity, constructing different types of spatial relationships. These relationships then go on to reflect in actual music. Although the goals of Tonal Pitch Space theory are also to enable a deeper understanding of musical prolongation and other facets pertaining to generativity in music, this is also a way of geometrically laying out possibly cognitive pitch structures.

In his work, \cite{mazzola} Guerrino Mazzola explains different forms of musical topographies and how we should think about them. These topographies are simple abstractions of high dimensional musical references, and given as that is, how does the restructuring of geometrical shape change the topographic and musical allusions. 

There have also been some attempts \cite{bhattacharya}, \cite{SOM}, to lay out and map tonal spaces in Hindustani music, although these do not suggest the topographical layouts of individual ragas and raga-types.
As a cultural musicology student, it is natural to ask, how do geometric structures hold up in cultural musics of different places. Exploring the geometric possibilities of Indian music with relation to force directed graph would be handled in Chapter 4 of this book.

\section{Hindustani Music}

Hindustani music is a primarily melodic form of music. Indigenous instruments and vocal styles are used in the performance of this style. This form is based on the idea of a Raga, which is a melodic abstraction containing some rules for notes and transitions. Many sophisticated ways to capture melodic nuance are essential to the study and analysis of Hindustani music. It is an improvised form, where the base poetry is typically sing in a theme-and-variation style. This form of music has emerged from the music of the subcontinent several hundred years ago, although \textit{khyal} music, which is prevalently practised today, owes its invention to persian and mughal influences and developments from the 14th century. 

Hindustani music is indigenous to the north of India, while in the South, the style of music is known as Carnatic or Karnatak classical music. There are several differences in the melodic and textual development as well as the forms of performance.

In Hindustani music, the voice is one of the most dominant features. Mostly all instruments are played in the range of the voice, with few instruments ever exceeding about 3 or 4 octaves. Even instrumentalists are taught to sing. Singers or soloists are accompanied by the primary rhythmic instrument called 'tabla', which plays a consistent hypermetric rhythm having poetic 'bols' or phonemes that are produced on the hand percussion instrument. There are no key changes and modulations in Hindustani music, and all music is always sung in one tonic key. To establish this tonic, a resonant drone is constantly played at the back with an instrument called the '\textit{tanpura}' or 'tambura'. This sound forms the harmonic base of the music.

Sophisticated melodic operations distinguish this music from all other styles. Vocal nuances of melody are captured through different sections such as 'alaap', 'taan', 'behlava', 'gamak', and different types of these parts. The sung body of text is ornamented heavily. The raga allows for several types of pitch combinations and phrases, but also disallows several others, making it a complex style to learn. Most of the material in a Hindustani concert is elaborated on the spot, with free improvisation.

Performance and training in Hindustani music almost always involves hand gestures.

\subsection{Gesture in Hindustani Music}
The construction and use of spontaneous hand gestures in the performance of Hindustani music has been explained using detailed interviews and observation through many studies in the last decade \cite{clayton2005time} \cite{clayton2001time} \cite{dahl2009gestures} \cite{leante2009lotus} \cite{clayton2011imagery} \cite{clayton2013experience} \cite{rahaim2008gesture} \cite{pearson2013gesture}. Hindustani music relies
on far fewer held and elongated notes, but more on free ornamentation and flowing motivic melodic patterns. These spontaneous melodic patterns are always accompanied by hand gestures. These hand gestures are not culturally codified, and are not iconic, but signify something else.

The analysis of similar spontaneous gestures in Carnatic music during teaching has also explored the minutiae of guidance in cognitive function for perception of pitch patterns \cite{pearson2013gesture}. The role of hand motions in Hindustani music pertains to the visualization of melodic contours, and therefore it becomes a tool for structuring music as such.

Instruction in Hindustani music is also accompanied with gestures about the motivic and motional qualities of the raga, explaining its nature and behavior. Performer gestures and interviews investigate the nature of this gesturing thoroughly in the aforementioned works. There is also a strong indication of how this co-occurrence might be influenced by raga \cite{rahaim2008gesture}, or some small units of raga. 

There have also been some studies investigating shapes in Hindustani music in the form of sound tracings \cite{roytrap}. 

\subsection{Analysis of Gesture in HCM}
There is sufficient documentation to suggest that the conceptualization of spatial objects in music occurs right from the time of starting to study. Teachers teach with several expressive hand gestures to exactly get the motional metaphor across to students that they teach. A typical lesson consists of a student imitating the phrases created by the teacher. When this phrase is not well understood, the teacher will often repeat the phrase several times, and explain it with nuanced hand gestures till the student is able to repeat both the phrase as well as understands the spatial context in which the phrase is created. 

Observational analysis of hand gestures in HCM has also been important to understand the relationships of these gestures with mental models and mental imagery. In \cite{clayton2005time}, veteran singer Veena Sahasrabuddhe explains the ways in which her modes of conceptualizing improvisation are linked with shapes that occur spontaneously to her mind. 

\section{Contributions}
Chapter 2 of this thesis deals with understanding and analysis of music-accompanying gestures by video recording and other methods. I annotated these videos for occurrences of hand shapes and discuss the findings. The analysis of video recordings is compared with musical features to understand the dependence of gestures on sonic features. Since Hindustani music consists of language as a sonic feature too, as the pieces are sung poetry, we analyse this in the context of the prosodic construction of the spoken text of the khyals. We then analyse the similarities and differences of these gestures from speech gestures in Hindi.

Chapter 3 of this thesis deals with sound tracings. I present experiments in which mixed sound stimuli containing raga phrases are presented to participants to trace. The responses are captured and analysed, and we present a system to generate raga phrases from these tracings. 

Chapter 4 of the thesis deals with Tonal pitch space representations of raga. I introduce a graph theory based method to organize raga categories and different notes in music. These are spatially arranged using a force direction algorithm, yielding a rich topographical analysis of the notion of 'proximity' of ragas.

I have tried to present an analysis of the interaction of music and abstract shapes, reflected through hand gestures of performers, and sound tracings by listeners, and tonal maps of larger musical structures.




