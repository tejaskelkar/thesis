\section{Introduction}
In the previous chapter, we saw how constraint satisfaction models can be used for generating new musical material. In Chapter 3, we work on it using sketches as the backbone on which to rest the frame of phrasal composition. The categories from which phrases can be derived can be several, and the constraints could be extracted to map several other features. In this chapter, we visualize an ontology of such categories using graph creation and layouting. We propose a tool that will help output several affective categories of tonal spaces. 

In his book \cite{lerdahl2001tonal}, Lerdahl explores pitch tone spaces as a means to closely map chord progressions and distances to come up with a cognitive model of pitch space that represents the cognitive distance or the ease of traversing a pitch space. The proximity in these maps is represented by physical proximity of categories. We propose a similar map for Hindustani music through this experiment. 

Ragas are described as near and far in several important works and also in common parlance. This proximity is represented by feature distance, but an approximate space for feature categorization is not known. Through this work, we represent these features using graph visualization algorithms. 


\section{Experiment 1: Creating pitch tone space graphical models}
A method to visualize these tonal spaces has potential applications in computer music for Hindustani Classical Music (HCM), and also for the effective study of relationships between tonal clusters and the spectra of their emotional affect.

HCM is a rich area to interact with tonal concepts. This music, based out of melody has a central concept of ‘raga’. The theory of raga is similar to some aspects of Western tonal composition, giving precedence to the tonic, dominant and subdominant notes, arranging 12 notes of music into smaller groups and scales, and definite models of melodic abstraction with concepts of cadences, themes and variation, and so on \cite{hindsangpad}. Several methods of deriving modes in Western music right from the Greek modes to the modes of limited transposition and so on use the combinatorial spaces of modal composition. The notion of nearness in the description of tonal spaces is common to HCM. Teachers often refer to similar ragas as being proximate, depending on several factors \cite{hindsangpad}, including the index of modification of individual notes, the abstraction from heptatonic to hexa- and pentatonic spaces and many other similarities. 

We contribute a method of analyzing raga spaces from the point of view of tonality, giving a rich description of an alternate view of the tonal space. We describe a method of creating combinations of modes through the point of view of colour harmony and the colour wheel. A brief description of the work can be found in fig \ref{fig:ragadescriptors}
\begin{figure}
\centering
\includegraphics[height=4cm]{figures/chp4/raagdescriptors.png}
\caption{Brief description of work}
\label{fig:ragadescriptors}
\end{figure}

\subsection{Related Work}

The idea of mapping visual spaces for pitch representation has existed for long. In western theory, diagrams starting from the circle of fifths on to higher dimensional \cite{lerdahl2001tonal} and topological pitch spaces \cite{mazzola} have been representative of the cognitive structures of music \cite{farbood}.

Graph-based methods for musical analysis are important for various applications. Graph based models are used for applications in evolutionary generative music \cite{generativemusgraph}. The field of music information retrieval also uses graph models \cite{mirgraph}. 
\cite{kramikpustakmalika} presents an analysis of topological frameworks for modes of limited transpositions. The authors in \cite{colorwheel} exploit the notion of parsimonious graphs with vertices being triads and 7th chords. In \cite{narayani}, a graph topological method for analysis of melodies is presented. This lets us view the dominant and repetitive characteristics of a musical piece as a graph. 

Visualization of tonal spaces of ragas in particular has been handled in the paper titled Modeling and Visualizing Tonality in North Indian Classical Music  \cite{SOM}. The authors propose a self organizing map (SOM) based system to visualize inter-tonal spaces of ragas and thaats. The approach towards a formulation of data for this study is through information retrieval, rather than a framework inspired from theoretical quantification. In this paper, we explore the relationships between tone spaces  Correlation of color and music is also explored through the means of emotional affect in \cite{probetone}, \cite{musiccolor} and other studies. 
In addition to the grammatical relationships of tones and graphs, tone space visualization is also important to analyze musical expectation. In a cross cultural study about musical expectation, probe tone ratings for musical expectation in raga were shown to be highly correlated with the distribution of pitch classes \cite{probetone}. This suggests a mental model of tonal percepts, creating functional musical expectations in raga similar to chordal harmonic functions.  Psychological experimentation with ragas and their affective emotional and cognitive properties is hindered by the lack of robust methods of organizing them into clusters, subtypes or even predicting distances between different tonal schemes. Laying out how tonal spaces behave in a quantifiable manner is thus a step further in the process of mathematically being able to portray the relationship between tones and affect.



\subsection{Raga Concepts}
A raga can be thought of as a mode with grammars. Constituent notes are the most important property of ragas, but those alone are not enough to describe raga. There are also rules regarding the use and disuse of notes during ascending and descending phrases, the importance of few notes and the restrictive use of others, along with specific methods of ornamentation. Hindustani music is always performed in relative pitch terms, which means that there is no description and requirement of absolute pitches, and these relative pitch terms are sufficient descriptors.

\subsubsection{Representation of Notes}
There are 12 notes in HCM, and just like the sol-fa system, with named 7 notes in the major scale, and 5 notes having  names with modifiers. These notes are:Sa, Re, Ga, Ma, Pa, Dha, Ni. The tonic, perfect fourth and perfect fifth (Sa, Ma, Pa) are un-alterable, and the rest have forms with accidentals (R, G, M, D, N). While R, G, D, N can have a flat accidental, only M can have a sharp (tritone). These flat notes are named ‘komal’ (soft), and are represented thus: r, g, d, n. The row of twelve pitches is the following in the HCM way of representation: S, r, R, g, G, m, M, P, d, D, n, N. This is how the notes have been coded for the purpose of this graph.

There are no key modulations in HCM and to perform, one can begin on any comfortable base tonic, and build up the other notes from there. Hierarchy of tones is also described for ragas as mentioned below. To capture some further nuance in raga description, ascending and descending scales are often given, capturing the typical upward and downward motions that the phrases define. 
\subsubsection{Descriptors of raga: Thaat}
There are hundreds of ragas with unique melodic rules. There have been many ways of classifying ragas into smaller groups such as the raga-ragini style, which described ‘male’ and ‘female’ ragas. The most prevalent method of classification now is the ‘thaat’ method, which describes 10 groups of 7 notes each, and the ragas show belongingness to any 1 of these 10 groups based on melodic affinity and other characteristics.
Table \ref{table:thaats} represents all thaats along with their constituent notes. The binary coding represents the presence or absence of each of the 12 notes. Thaat is the most important descriptor that will be used in this study.

\begin{table}
\centering
\begin{tabular}{llll}
\hline
Thaat    & Notes               & Binary Coding & Western Mode \\
\hline
Bilaval  & S, R, G, m, P, D, N & 101011010101  & Major        \\
Kalyan   & S, R, G, M, P, D, N & 101010110101  & Lydian       \\
Khamaj   & S, R, G, m, P, D, n & 101011010110  & Mixolydian   \\
Kafi     & S, R, g, m, P, D, n & 101101010110  & Dorian       \\
Asavari  & S, R, g, m, P, d, n & 101101011010  & Minor        \\
Bhairav  & S, r, G, m, P, d, N & 110011011001  & Hitzazkiar   \\
Poorvi   & S, r, G, M, P, D, N & 110010110101  &              \\
Todi     & S, r, g, M, P, d, N & 110100111001  &              \\
Marva    & S, r, G, M, P, d, N & 110010111001  &              \\
Bhairavi & S, r, g, m, P, d, n & 110101011010  & Phrygian    
\end{tabular}
\caption{Thaats and their descriptors}
\label{table:thaats}
\end{table}

\subsubsection{Other Descriptors of raga}
Other than the constituent notes, there are several features that are described in raga theory. Some of these are listed below:
\begin{enumerate}
\item Vadi (tenor), Samvadi (second tenor) notes – the vadi note is defined as the most important note in the raga, the samvadi being second most important. The vadi is also sung most frequently and the samvadi is the next most prevalent.
\item Nyas (final) notes – these are notes that occur at the end of a melodic phrase. They are the cadential notes at which the phrase is permitted to finish.
\item Anuvadi (optional), Vivadi (disputed) notes – Anuvadi notes are the ones used for beautification occasionally in a raga, whereas vivadi notes are even rarer.
\item Varjit (omitted) notes – these notes are never used in raga rendition.
\item Jati (family) – Not all 7 notes are picked up in every raga. The following table describes the family of a raga based on the number of notes in ascent and descent. Table \ref{table:jati} represents these classes.

\begin{table}[h]
\centering
\begin{tabular}{ll}
\hline
Jati (Family) &            \\
\hline
Audav         & Pentatonic \\
Shadav        & Hexatonic  \\
Sampoorna     & Heptatonic
\end{tabular}
\caption{Jati - families of raga based on number of notes}
\label{table:jati}
\end{table}

\item Time of the day - this is an ancient practice that has carried out to current times. Different thaats are assigned to different time zones of a 24 hour cycle. This might be relevant from the point of view of a vocalist, given as the voice is more easily accustomed to lower notes in the morning than in the evening. We use this information in the dataset as well. 
\end{enumerate}

\subsection{Ragas as Graphs}


\subsubsection{Raga Dataset and Descriptors}
Our dataset contains 163 commonly sung ragas. All notes are coded as described in section 1.2. The descriptors described in section 1.2 are used for this coding. The present network has a total of 393 nodes and 7,010 edges. This information is sourced from the seminal authoritative books on Indian music theory \cite{kramikpustakmalika} \cite{hindsangpad}. 

\subsubsection{Graph Construction}
In the first step, all unique entities are considered to be independent nodes for the creation of this graph. Edges are created amongst these nodes. We create a directed graph with edges directed outwards from the parent node (eg raga) to the attribute (eg tenor note). The edge type is the property of the target node. For example, the edge between a raga x containing a thaat y is labeled as ‘y’.  Edges were computed between each participating note in the raga as well as notes found in the short description phrase in a raga. The order of these notes was also preserved. The ascent and descent phrases were ordered with a letter and a number, linking the edges to the notes that are found many times in the ascent, descent and the characteristic phrase. The weights were distributed for the notes based upon edge type. If a note happens to be the tenor (Vadi) of a raga, it is assigned a larger weight than if it is an optional note (anuvadi). Weights are also assigned based on the degree of connectivity.


\subsubsection{Graph Layout}
We used three algorithms: the Fruchterman Reingold, ForceAtlas2 and Yi Fan Hu algorithms for laying out this dataset. We obtain a total of 10 clusters with a resolution of 0.68, and a modularity of 0.185. The average degree of the network is 4.751 and the average weighted degree is 18.704 with a sparse graph density of 0.012. 
We processed this graph in the graph layout and analysis tool, Gephi \cite{gephi}. While the Fruchterman Reingold and Yi Fan Hu \cite{yifanhu} algorithms are better for representations without overlap, the clusters of notes is directed inwards and it is hard to observe the note cluster relationships with raga nodes. We use the Force Atlas 2 \cite{forceatlas} algorithm for presenting the results visually. We lay the graph out with high gravity and repulsion, thus creating tension between the nodes. The best results are with a large scaling parameter of 5.0 and an approximate repulsion of 1.2. We tweaked the graph at a slow incremental rate per iteration of 0.2. We use the Prevent Overlap mode, with Edge weight influencing the visualization of the graph. 


\subsection{Algorithm}

In force-based layout systems, a initial vertex placement is modified by continuously moving the vertices according to a system of forces based on physical metaphors related to systems of springs or molecular mechanics. Typically, these systems combine attractive forces between adjacent vertices with repulsive forces between all pairs of vertices, in order to seek a layout in which edge lengths are small while vertices are well-separated. These systems may perform gradient descent based minimization of an energy function, or they may translate the forces directly into velocities or accelerations for the moving vertices. We use the Force Atlas Algorithm as described below for visualization:

\paragraph{Energy Model}
The energy model of this algorithm uses a repulsion force equal to the repulsion for electrically charged particles which is $(F_{r} = k/d^2)$, and an attraction force describing springs. $F_{a} = -k.d$.

\paragraph{Attraction Force}
The attraction repulsion of ForceAtlas takes up the values  (1, −1). Visual clusters denote structural densities when attraction − repulsion (a-r) is low, that is when the attraction force depends less on distance, and when the repulsion force depends more on it. The starting attraction force for Force Atlas model is simply $F_{a}(n_{1}, n_{2}) = d(n_{1}, n_{2})$, which is linearly dependent. 

\paragraph{Repulsion by Degree}
This algorithm tries to bring together the nodes which dno't have many connected edges. This means that even if the degree is small, the attraction towards the main clusters should be sufficient.
In this formula, the repulsion is proportional to the degree+1. The coefficient k can be adjusted. 

$F_{r}(n_{1}, n_{2}) = k_{r} \frac{(deg(n_{1}) + 1)(deg(n_{2}) + 1)}{d(n_{1}, n_{2}) }$

\paragraph{LinLog Mode}
We use a logarithmic attraction force. This means that the attraction is defined by 
$F_{a}(n_{1}, n_{2}) = log(1 + d(n_{1}, n_{2}))$ 
rather than $F_{a}(n_{1}, n_{2}) = d(n_{1}, n_{2})$.

\paragraph{Gravity}
Gravity is defined as the force that keeps islands near the main body. This value is also like the repulsion force but with a separate constant, acting upon nodes that have degree 0.
$F_{g}(n) = k_{g}(deg(n) + 1)$

\paragraph{Scaling}
Graph scaling in this case can be carried out by adjusting the constants $k_{g}$ and $k_{r}$.
$F'_{g}(n) = k_{g}(deg(n) + 1)d(n)$

\paragraph{Edge Weights}
We use the edge weight parameter, with weight w(e) influences the attraction force by the formula:

$F_{a} = w(e)^{\delta}d(n_{1}, n_{2})$ 

\paragraph{Hub Dissuasion}
Dissuading hubs grants a more central position to nodes that are very well connected. We want this to happen in the proposed graph as interconnections are many and the most popular nodes should be at the least distance. For this, we make $F_{a}$ inversely proportional to degree+1 as follows: 
$F_{a}(n_{1}, n_{2}) = \frac{d(n_{1}, n_{2})}{deg(n_{1}) + 1} $


In the final layout found in \ref{fig:fullgraph}, we find the nodes with the highest degree to be located nearer to the center, with sparsity increasing radially outwards. We find that clusters of notes come closer based upon their frequent use together. The parent thaats to which ragas belong also separate out to separate angles around the circle. Detailed analysis is presented below.

\section{Analysis of Graph}

\begin{figure}
\centering
\includegraphics[height=8cm]{figures/chp4/fullgraph.png}
\caption{The complete network of ragas}
\label{fig:fullgraph}
\end{figure}

The final layout is seen in Fig \ref{fig:fullgraph}. As expected, the graph is centered around the tonic ‘Sa’, which is an attribute of every raga node. The close neighbors of ‘Sa’ are ‘Pa’ and ‘Ma’, the fifth and fourth scale degrees respectively, either of which must be present to complete a raga. 

 
\subsection{Conceptual Distances in Tone Spaces}
 

\begin{figure}
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.9]{figures/chp4/conceptdist.png} & \includegraphics[scale=0.9]{figures/chp4/times.png}\\
(a) & (b)
\end{tabular}
\caption{Conceptual distances between diatonic notes (a) and Thaats visualized according to time of the day (b)}
\label{fig:conceptualdistances}
\end{figure}

There are several ways in which this graph layout corroborates with the conceptual notion of distance in HCM.
As seen in fig \ref{fig:bhairavnarayani}, the following observations can be made:
\begin{enumerate}
\item In HCM it is suggested that ragas have a ‘S-P’ or ‘S-m’ bhaav, or a centrality around P5 or P4. There cannot be a raga with both a P4 and P5 missing. We find this triad of S-P-m right in the center of the graph. Fig \ref{fig:conceptualdistances}. Shows the final layout of the constituent notes, sizes in decreasing order of prevalence.

\item The interval of a minor second is considered dissonant. We find that the distance of different forms of the same note in the graph is the furthest. In the graph, these nodes are furthest away from each other. 

\item Thaat clusters are formed around two pairs of notes that go together. In HCM the 3rd and the 6th, and the 2nd and 4th scale degrees are described to have a natural affinity. In the graph too, these two pairs of notes appear together. The thaats containing these combinations are distributed around them. Fig 4 and 5 have some examples of ragas with similar properties together.

\item Overall it can be stated that there is a larger variance and diversity in the ragas which have flat notes or accidentals, while the ones having tones from a major scale are huddled much closer together. 
	
\item It is also possible to visualize which family (Penta / Hexa / Heptatonic) is most prevalent, and which is prevalent amongst which thaat. 
Thus we observe that many latent underlying principles that explain the harmonic relationships between notes are consistent in this layout. 
\end{enumerate}



\begin{figure}
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.2]{figures/chp4/bhairav.png} & \includegraphics[scale=0.2]{figures/chp4/proximateraags.png} \\
(a) & (b) \\
\end{tabular}
\caption{Bhairav family ragas come together (a), Gorakh Kalyan and Narayani; Desh and Tilak Kamod – pairs of similar ragas \cite{narayani}(b) }
\label{fig:bhairavnarayani}
\end{figure}
	 
 
\subsubsection{Time of the Day and Thaat}

 \begin{figure}
\centering
\includegraphics[height=4cm]{figures/chp4/thaathdist.png}
\caption{Thaath space layout}
\label{fig:thaath}
\end{figure}

The plot for the thaats constituting HCM can be seen in in \ref{fig:thaath}. An edit distance of 1 for the consecutive notes, separates the thaats as we go along any direction along the circle. This corroborates with Jairazbhoy's thaath map presented in his book \cite{jairazbhoy}. These thaats in the graph go at radially outwards, thus having a specific tonal colour situated at different angles radially outward. Fig \ref{fig:conceptualdistances} displays the arrangement obtained by displaying the times of the day for ragas as explained in section 1.2. 

We use this final graph layout to map to colors and thus derive other relationships of raga. 


\section{Mapping to Color Wheel}
\begin{figure}
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.2]{figures/chp4/hsv.png} & \includegraphics[scale=0.2]{figures/chp4/color-raagcomparison.png} \\
(a) & (b) \\
\end{tabular}
\caption{HSV wheel for color relationships (a) and Mapping Ragas to Color wheel relationships (b)}
\end{figure}
	 
The study of colour harmony is closely related to the study of the nature of light and colour. Early layouts of colour wheels and colour harmony are attributed to Newton, Goethe and others. Modern colour theory also deals with colour representations and colour harmony \cite{colorprimer}, \cite{itten}. Harmony is defined mostly as a systematic ordering of colour from these representations. \cite{matsuda}, \cite{colorschemer} are also studies of representing color harmonies quantitatively using the Ostwald colour system. Itten \cite{itten} discusses a color wheel emphasizing on hue. This theory is based on the relative positions of hues. This study presents a colour wheel of 12 colours derived from the primary colours. He referred to complementary colours as a two-colour harmony, and then discussed three-colour harmony, and colour harmony containing more vertices belonging to equilateral polygons. 

There are many interactive tools that provide designers with harmonic sets of colours (e.g., \cite{colorschemer}, \cite{colorwheel}). Such applications provide the user with sets of harmonic colours accommodating specifications of colour seeds and other parameters.
A.	Properties of Colour Wheels
In this paper, we use the HSV model for representing colours as seen in Fig X. This layout has Hue, Saturation, Value parameters, with hue spread around the angle of the circle, its saturation varying radially outwards, and the value given by a separate axis.

This means that in a subtractive colour scheme, the center of this circle is white, with colours all around it. The colours are dispersed in their occurrence along the light spectrum, with the primary colours occupying the triadic positions, separated at 120 degrees from each other. This forms the circular colour wheel as show in Fig. 8. The vertical axis is used to determine the value of the colour, but we wouldn’t be using that parameter for this study.

\subsection{Colour Schemes}
Using the wheel described above, several sets of colour schemes can be derived. Some of these relationships are as given below:
 
 \begin{figure}
\centering
\includegraphics[height=4cm]{figures/chp4/colorrelationships.png}
\caption{Representations of color harmony relationships}
\label{fig:colorharm}
\end{figure}

\begin{itemize}
\item Monochromatic
 colour schemes or i-type colour schemes are in one direction radially outwards.
\item Analogous 
 or v-type colour schemes are spread over an arc radially outwards. As this scheme covers an arc, it is possible to capture colours that have similarity relationships with the colour picked. 
\item Complementary
 colours lie diametrically opposite to each other. Also called the I-type colour scheme.
\item Y Type
scheme includes a narrow i-type colour scheme on one side, and a diametrically opposite large angle on the other side.
\item Triadic
 scheme uses colours that are evenly spaced around the colour wheel in an equilateral triangle.
\item Polychromatic
\end{itemize}



\section{Raga wheel and color harmony}
\subsection{Deriving affective Combinations}
We use this method of colour harmony mapping to derive several relationships between ragas. These relationships correspond to the colour harmony relationships defined in section V (B).

\begin{figure}
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.2]{figures/chp4/monochromatic.png} & \includegraphics[scale=0.2]{figures/chp4/analogous.png} \\
(a) & (b) \\
\end{tabular}
\caption{Monochromatic Ragas (a), Analogous Ragas (b) }
\end{figure}
	 

\subsubsection{Monochromatic}


In the above example, the notes in these ragas radially outwards is moving towards a purer description of the saturated thaat ‘Bhairavi’ (Phrygian Mode). The exact note numbers are described below.

\begin{table}[h]
\centering
\begin{tabular}{lllll}
\hline
ID & raga         & Binary       & Thaat    & Time    \\
\hline
1  & Kirvani      & 101101011001 & None     & Any     \\
2  & Ahiri        & 110101011010 & Bhairavi & 6-9 am  \\
3  & Chandrakauns & 100101000110 & Bhairavi & 12-3 am \\
4  & Malkauns     & 100101001010 & Bhairavi & 3 am    \\
5  & Bhairavi     & 110101011010 & Bhairavi & 3-6 am 
\end{tabular}
\caption{Monochromatic ragas}
\label{table:monochrome}
\end{table}

The relationship between these ragas can be described as ‘monochromatic’, where a similar thaat colour has been evoked. All these ragas are described as being ‘peaceful, serious and quiet’ - variations of the same affect.

\subsubsection{Analogous}
 Analogous ragas were found to be the ones that were close to each other along a radial arc from the center. The angle of such an arc is usually small, and the neighboring nodes along this arc are the required analogous groups. 

\begin{table}[h]
\centering
\begin{tabular}{llll}
\hline
Parameter           & Hamsadhwani  & Chandrakant      & Difference  \\
\hline
Ascent              & S,R,G,P,N,S' & S,R,G,P,D,N,S'   & 1 note      \\
Descent             & S',N,P,G,R,S & S',N,D,P,M,G,R,S & 2 notes     \\
Binary Constituents & 101010010001 & 101010110101     & 00000010100 \\
Family (Jati)       & Pentatonic   & Hexa-Heptatonic  &             \\
Time                & 9pm-12pm     & 9pm-12pm         & Same        \\
Thaat               & Bilaval      & Bilaval          &            
\end{tabular}
\caption{Analogous ragas}
\label{table:analogous}

\end{table}
The analysis of analogous ragas thus reveals the relative closeness in the binary distances. We also find for several pairs that they occur in the same time period as their neighbors.

\begin{figure}
\centering
\begin{tabular}{cc}
\includegraphics[scale=0.2]{figures/chp4/triadic.png} & \includegraphics[scale=0.2]{figures/chp4/complementary.png} \\
(a) & (b) \\
\end{tabular}
\caption{Triadic Ragas (a), Complementary Ragas (b) }
\end{figure}
	 
	 

\subsubsection{Complementary}
This is an example of complementary ragas Darbari Kanada and Shuddha Kalyan. The description is below:

\begin{table}[h]
\centering
\begin{tabular}{llll}
\hline
Parameter           & Darbari                 & Shuddha Kalyan  & Difference   \\
\hline
Ascent              & S,R,g,m,P,d,n,          & S,R,G,P,D,S     & 4 notes      \\
Descent             & S,d,n,P,m,P,g,m,R,S     & S,N,D,P,M,G,R,S & 5 notes      \\
Binary Constituents & 101101011010            & 101010110101    & 000111101111 \\
Family (Jati)       & Hexatonic – Bent        & Penta-Hexatonic &              \\
Time                & Midnight                & Evening 6-9pm   &              \\
Thaat               & Asavari (Natural Minor) & Kalyan (Lydian) & 4 notes     
\end{tabular}
\caption{Complementary ragas}
\label{table:complementary}
\end{table}


We see that these ragas are highly dissimilar in terms of the constituent notes. While the musical affect of Darbari is described as serious, regal and temperamental, Shuddha Kalyan has the effect of being soothing and calm. 
This is thus an effective way to get complementary pairs of ragas that are maximally separated. 

\subsubsection{Triadic}
Triadic ragas are derived from overlaying an equilateral triangle over the raga wheel. The analysis of these ragas is given below:


\begin{table}[h]
\centering
\begin{tabular}{llll}
\hline
Parameter & Bilaskhani Todi & Kamod               & Dhani            \\
\hline 
Ascent          & S,r,G,P,d,S'        & S,R,P,m,P,D,P,N,D,S  & 'n,S,g,m,P,n,S \\
Descent         & S’,r’,n,d,m,g,r,g,r,S &    S’,N,D,P,M,P,D & S',n,P,m,g,R,S \\
                						   &   & P,G,m,P,G,m,R,S &          \\
Time            & 6 - 9 am            & 6-9pm            & Day or Night  \\
Family          & Hexa-heptatonic     & Hexa-heptatonic  & Pentatonic    \\
Thaat           & Todi (2b,3b,4\#,6b) & Kalyan (Lydian)  & Kafi (Dorian)
\end{tabular}
\caption{Triadic ragas}
\label{table:triadic}

\end{table}

These examples illustrate the complex meta-level relationships that can be used to determine the best possible sequence of ragas to pick for a performance. The constraints of performing a particular family of ragas for a particular time frame can also be met under this system as the graph includes the time of the day. The ragas picked from such a study are also useful to combine for new compositions and automatic composition systems, where similarity measures between note groups and their affect is an important consideration. 

The mapping between raga space and colour is useful to develop dynamic visualization applications involving tone recognition. We provide one such mapping through this study.
B.	Future Work
The approach towards parameterization of a raga using the descriptors of scale degrees as well as weighting the importance of tones can be used for the clustering and visualization of not just ragas but various other modes, chords and harmonic styles and musics from other parts of the world. 
This layout for mapping ragas might represent the underlying cognitive properties of tonal structures in HCM. Further studies can be carried out to compare the layouting in this study with ratings and feedback from listeners of HCM for arranging tone spaces. Further experiments about the cognitive distinction of tonal spaces can be conducted using the mapping strategies used in this study. The resulting layouts can also be used in applications such as constructing groups for experiments regarding the affective emotional understanding of tonal spaces and ragas. Analyzing relationships through colour-harmony like measures is also one of the possible ways of mapping sound and colour in HCM for creation of sound art and other artistic explorations.

\section{Conclusion}
This study suggests that visualization of ragas through their representation as graphs facilitates their grouping, arrangement and matching to cognitive properties. Several analogous relationships between ragas can be derived from this final layout.


