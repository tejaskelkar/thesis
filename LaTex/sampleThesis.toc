\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Introduction}{1}
\contentsline {section}{\numberline {1.2}Thinking about Music and shape}{2}
\contentsline {subsection}{\numberline {1.2.1}Gestures in other musical styles}{3}
\contentsline {subsection}{\numberline {1.2.2}Conceptual Metaphors and Gesture}{4}
\contentsline {subsection}{\numberline {1.2.3}Musical Gesture and Semiotics}{5}
\contentsline {subsection}{\numberline {1.2.4}Neurological evidence for music spatialization}{5}
\contentsline {section}{\numberline {1.3}Sound Objects and Action-Sound}{6}
\contentsline {section}{\numberline {1.4}Tonal Pitch Space}{7}
\contentsline {section}{\numberline {1.5}Hindustani Music}{8}
\contentsline {subsection}{\numberline {1.5.1}Gesture in Hindustani Music}{9}
\contentsline {subsection}{\numberline {1.5.2}Analysis of Gesture in HCM}{9}
\contentsline {section}{\numberline {1.6}Contributions}{9}
\contentsline {chapter}{\numberline {2}Analysis of Gesture}{11}
\contentsline {section}{\numberline {2.1}Introduction}{11}
\contentsline {subsection}{\numberline {2.1.1}The structure of \textit {Khyal}}{12}
\contentsline {section}{\numberline {2.2}Survey of Gestural Use}{12}
\contentsline {subsection}{\numberline {2.2.1}Responses to Survey on Gesturing to Music}{12}
\contentsline {section}{\numberline {2.3}Video Recording Dataset}{14}
\contentsline {subsection}{\numberline {2.3.1}Gestural Notation}{14}
\contentsline {subsection}{\numberline {2.3.2}Terms indicating motion metaphors}{17}
\contentsline {subsection}{\numberline {2.3.3}Experiment 1: Analysis of Video Capture}{18}
\contentsline {subsubsection}{\numberline {2.3.3.1}Methods}{18}
\contentsline {subsubsection}{\numberline {2.3.3.2}Correlational Analysis of Pitch}{19}
\contentsline {subsubsection}{\numberline {2.3.3.3}Results and Inferences}{20}
\contentsline {subsection}{\numberline {2.3.4}Experiment 2: Annotation of Gesture}{20}
\contentsline {subsubsection}{\numberline {2.3.4.1}Methods}{21}
\contentsline {subsection}{\numberline {2.3.5}Observations about Musical Events}{21}
\contentsline {paragraph}{\numberline {2.3.5.0.1}Pause}{21}
\contentsline {paragraph}{\numberline {2.3.5.0.2}Height gesture and Pitch Height representation}{22}
\contentsline {paragraph}{\numberline {2.3.5.0.3}Use of Extreme Periphery}{22}
\contentsline {paragraph}{\numberline {2.3.5.0.4}Musical Object Manipulation}{22}
\contentsline {paragraph}{\numberline {2.3.5.0.5}Theme and Variations}{23}
\contentsline {paragraph}{\numberline {2.3.5.0.6}Precedence of Rhythmic Ornaments over Melodic}{24}
\contentsline {subsubsection}{\numberline {2.3.5.1}Inferences}{25}
\contentsline {paragraph}{\numberline {2.3.5.1.1}Typology of Gestures}{25}
\contentsline {paragraph}{\numberline {2.3.5.1.2}Rhythmic thinking}{25}
\contentsline {paragraph}{\numberline {2.3.5.1.3}Gesture and Working Memory}{26}
\contentsline {paragraph}{\numberline {2.3.5.1.4}Articulations and Prosodic Content}{26}
\contentsline {section}{\numberline {2.4}Gesture, Prosody and Music}{26}
\contentsline {subsection}{\numberline {2.4.1}Experiment 3: Prosodic Stress and Composition}{26}
\contentsline {paragraph}{\numberline {2.4.1.0.5}Prosody in speech}{26}
\contentsline {subsubsection}{\numberline {2.4.1.1}Prosody in Hindi - Previous findings}{26}
\contentsline {subsubsection}{\numberline {2.4.1.2}Methods}{27}
\contentsline {paragraph}{\numberline {2.4.1.2.1}Experiment}{27}
\contentsline {subsubsection}{\numberline {2.4.1.3}Results}{28}
\contentsline {subsubsection}{\numberline {2.4.1.4}Inferences}{28}
\contentsline {subsection}{\numberline {2.4.2}Experiment 4: Presenting Gesture in Spoken Hindi}{29}
\contentsline {subsubsection}{\numberline {2.4.2.1}Methods}{30}
\contentsline {subsection}{\numberline {2.4.3}Results}{31}
\contentsline {subsection}{\numberline {2.4.4}Inferences}{31}
\contentsline {section}{\numberline {2.5}Conclusions}{31}
\contentsline {chapter}{\numberline {3}Music and Shape}{33}
\contentsline {section}{\numberline {3.1}Introduction}{33}
\contentsline {section}{\numberline {3.2}Experiment 1: Graphical Notation}{33}
\contentsline {subsection}{\numberline {3.2.1}Method}{34}
\contentsline {subsection}{\numberline {3.2.2}Results}{35}
\contentsline {section}{\numberline {3.3}Experiment 2: Sound Tracing}{36}
\contentsline {subsubsection}{\numberline {3.3.0.1}Stimuli Design}{37}
\contentsline {subsection}{\numberline {3.3.1}Experimental Setup}{38}
\contentsline {subsection}{\numberline {3.3.2}Qualitative Results}{38}
\contentsline {paragraph}{\numberline {3.3.2.0.1}Classifiable data across 4 categories}{39}
\contentsline {paragraph}{\numberline {3.3.2.0.2}Ascending Phrase Bias}{40}
\contentsline {paragraph}{\numberline {3.3.2.0.3}Articulation Representation}{40}
\contentsline {paragraph}{\numberline {3.3.2.0.4}Reproducibility of sound tracings for repeated stimuli}{41}
\contentsline {paragraph}{\numberline {3.3.2.0.5}Pitch verticality in tracings}{41}
\contentsline {paragraph}{\numberline {3.3.2.0.6}Ambiguity in referencing base notes}{41}
\contentsline {paragraph}{\numberline {3.3.2.0.7}Special Cases}{42}
\contentsline {subsection}{\numberline {3.3.3}Interface Design}{43}
\contentsline {subsection}{\numberline {3.3.4}Tracing Approximation}{43}
\contentsline {subsection}{\numberline {3.3.5}Phrase Classification}{44}
\contentsline {subsection}{\numberline {3.3.6}Tracing Segmentation}{46}
\contentsline {subsection}{\numberline {3.3.7}Phrase Generation}{46}
\contentsline {subsection}{\numberline {3.3.8}Raga Grammar}{47}
\contentsline {section}{\numberline {3.4}Conclusions}{49}
\contentsline {chapter}{\numberline {4}Tone Space and Generativity}{50}
\contentsline {section}{\numberline {4.1}Introduction}{50}
\contentsline {section}{\numberline {4.2}Experiment 1: Creating pitch tone space graphical models}{50}
\contentsline {subsection}{\numberline {4.2.1}Related Work}{51}
\contentsline {subsection}{\numberline {4.2.2}Raga Concepts}{52}
\contentsline {subsubsection}{\numberline {4.2.2.1}Representation of Notes}{52}
\contentsline {subsubsection}{\numberline {4.2.2.2}Descriptors of raga: Thaat}{52}
\contentsline {subsubsection}{\numberline {4.2.2.3}Other Descriptors of raga}{53}
\contentsline {subsection}{\numberline {4.2.3}Ragas as Graphs}{54}
\contentsline {subsubsection}{\numberline {4.2.3.1}Raga Dataset and Descriptors}{54}
\contentsline {subsubsection}{\numberline {4.2.3.2}Graph Construction}{54}
\contentsline {subsubsection}{\numberline {4.2.3.3}Graph Layout}{54}
\contentsline {subsection}{\numberline {4.2.4}Algorithm}{54}
\contentsline {paragraph}{\numberline {4.2.4.0.1}Energy Model}{55}
\contentsline {paragraph}{\numberline {4.2.4.0.2}Attraction Force}{55}
\contentsline {paragraph}{\numberline {4.2.4.0.3}Repulsion by Degree}{55}
\contentsline {paragraph}{\numberline {4.2.4.0.4}LinLog Mode}{55}
\contentsline {paragraph}{\numberline {4.2.4.0.5}Gravity}{55}
\contentsline {paragraph}{\numberline {4.2.4.0.6}Scaling}{55}
\contentsline {paragraph}{\numberline {4.2.4.0.7}Edge Weights}{55}
\contentsline {paragraph}{\numberline {4.2.4.0.8}Hub Dissuasion}{55}
\contentsline {section}{\numberline {4.3}Analysis of Graph}{56}
\contentsline {subsection}{\numberline {4.3.1}Conceptual Distances in Tone Spaces}{56}
\contentsline {subsubsection}{\numberline {4.3.1.1}Time of the Day and Thaat}{57}
\contentsline {section}{\numberline {4.4}Mapping to Color Wheel}{58}
\contentsline {subsection}{\numberline {4.4.1}Colour Schemes}{59}
\contentsline {section}{\numberline {4.5}Raga wheel and color harmony}{60}
\contentsline {subsection}{\numberline {4.5.1}Deriving affective Combinations}{60}
\contentsline {subsubsection}{\numberline {4.5.1.1}Monochromatic}{60}
\contentsline {subsubsection}{\numberline {4.5.1.2}Analogous}{60}
\contentsline {subsubsection}{\numberline {4.5.1.3}Complementary}{61}
\contentsline {subsubsection}{\numberline {4.5.1.4}Triadic}{61}
\contentsline {section}{\numberline {4.6}Conclusion}{62}
\contentsline {chapter}{\numberline {5}Appendix}{63}
\contentsline {section}{\numberline {5.1}Description of Hindustani Music Performances}{63}
\contentsline {section}{\numberline {5.2}Raga}{63}
\contentsline {section}{\numberline {5.3}Taal}{65}
\contentsline {section}{\numberline {5.4}Questionnaire Survey of Performer Gestures}{65}
\contentsline {chapter}{\numberline {6}Conclusions}{67}
{\@tempskipb 3.0ex plus 1pt\relax }
\contentsline {chapter}{{Bibliography}}{69}
